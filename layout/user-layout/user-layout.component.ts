import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { SlicePipe } from '@angular/common';
import { Http, Response, Headers } from '@angular/http';
import { api_endpoints } from '../../../app/api_endpoints';
declare var $;

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.component.html',
  styleUrls: ['./user-layout.component.css']
})

export class UserLayoutComponent implements OnInit {
  user_type: any = null;
  searchData = null;
  prodArray: any = null;
  searchBlankData = false;
  user: any = null;
  no_products = false;
  _loading = false;
  all_categories: any;
  search_data: any = null;
  sub_categories: any;
  all_products: any = null;
  cart_items_count: any = null;
  constructor(public _router: Router, private _http: Http) { }
  ngOnInit() {
    window.scrollTo(0, 0);

    // tslint:disable-next-line:max-line-length
    this.user = JSON.parse(window.sessionStorage.getItem('user_detail'));
    this.cart_items_count = window.localStorage.getItem('cart_data') === null ?
      '0' : JSON.parse('[' + window.localStorage.getItem('cart_data') + ']').length;
    this.user_type = this.user === null ? null : this.user.admin_type;
    this.getAllProducts();
    this.getAllCategories();
  }

  @HostListener('window:scroll') scrollHandler1() {
    console.log();
  }


  onSearchKey(evt) {
    if (evt.keyCode === 13) {
      this._http.get(api_endpoints + '/product/search_product?search_text=' + evt.target.value)
        .subscribe(data => {
          // this.prodArray = this._products(data.list, 4);
          this.searchData = null;
        });
    } else {
      this._http.get(api_endpoints + '/product/search_product?search_text=' + evt.target.value).
        subscribe(data => {
          if (evt.target.value === '') {
            this.searchData = null;
            this.searchBlankData = false;
            // } else if (data === 0) {
            //   this.searchData = null;
            //   this.searchBlankData = true;
          } else {
            this.searchData = JSON.parse(data['_body']).data;
          }
        });
    }
  }

  getAllCategories() {
    this._http.get(api_endpoints + '/get_categories')
      .subscribe(data => {
        this.all_categories = JSON.parse(data['_body']).data;
      });
  }

  getAllProducts() {
    this._loading = true;
    this._http.get(api_endpoints + '/get_all_products')
      .subscribe(data => {
        this.all_products = JSON.parse(data['_body']).data;
        this._loading = false;
      });
  }

  // Change Category
  changeCategory(evt) {
    this.getProducts(evt.target.value, 'category_id');
  }

  // Change Sub Category
  changeSubCategory(evt) {
    this.getProducts(evt.target.value, 'sub_category_id');
  }

  getProduct(evt) {
    this.getProducts(evt.id, 'product_id');
    this.search_data = evt.name;
    this.searchData = null;
  }

  getProducts(id, id_type) {
    this._loading = true;
    this._http.get(api_endpoints + '/product/get_products?' + id_type + '=' + id)
      .subscribe(data => {
        this.all_products = JSON.parse(data['_body']).data;
        if (id_type === 'category_id') {
          this.sub_categories = JSON.parse(data['_body']).sub_categories;
        }
        if (this.all_products.length === 0) {
          this.no_products = true;
        } else {
          this.no_products = false;
        }
        this._loading = false;
      });
  }

  onLogout() {
    window.sessionStorage.clear();
    window.location.href = '/';
  }

}
