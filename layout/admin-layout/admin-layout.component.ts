import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SlicePipe } from '@angular/common';
import { Http, Response, Headers } from '@angular/http';
import { api_endpoints } from '../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent implements OnInit {
  user_type: any;
  user: any;
  menus: any = [['Users', '/admin/users'],
  ['Products', 'admin/products'],
  ['Categories', 'admin/categories'],
  ['Sub Categories', 'admin/sub-categories'],
  ['Orders & Ordered Products', '/admin/orders'],
  ['Payments', 'admin/payments']];

  filterd_menu: any = [];
  constructor(public _router: Router, private _http: Http) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    $('.button-collapse').sideNav();
    this.user = JSON.parse(window.sessionStorage.getItem('user_detail'));
    this.user_type = this.user === null ? null : this.user.admin_type;
  }

  onSearchMenu(evt) {
    this.menus.filter(function (item) {
      return item.toLowerCase().includes(evt.target.value.toLowerCase());
    });
  }

  onLogout() {
    window.sessionStorage.clear();
    window.location.href = '/';
  }

}
