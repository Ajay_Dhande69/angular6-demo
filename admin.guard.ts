import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
@Injectable()

export class AdminGuard implements CanActivate {
  user: any;
  user_type: any;
  constructor(private _router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    this.user = JSON.parse(window.sessionStorage.getItem('user_detail'));
    this.user_type = this.user === null ? null : this.user.admin_type;
    if (this.user_type !== true) {
      Swal({
        title: 'Warning',
        text: 'Please sign in as an admin.',
        type: 'warning',
        confirmButtonText: 'Ok'
      }).then((result) => {
        return false;
      });
    } else {
      return true;
    }
  }
}
