import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.css']
})
export class SubCategoryComponent implements OnInit {
  categories: any = null;
  sub_category: any = null;
  _header: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getCategories();
    this.getSubCategory(this._activated_route.snapshot.params.sub_category_id);
  }

  // Get all Categories
  getCategories() {
    this._http.get(api_endpoints + '/category', this._header)
      .subscribe(data => {
        this.categories = JSON.parse(data['_body']).data;
      });
  }

  // Edit SubCategory
  getSubCategory(id) {
    this._http.get(api_endpoints + '/sub_category/' + id + '/edit', this._header)
      .subscribe(data => {
        this.sub_category = JSON.parse(data['_body']).data;
      });
  }

  // Create Sub Category
  onSubCategoryFormSubmit(data) {
    if (data.id === '') {
      this.create(data);
    } else {
      this.update(data);
    }
  }

  create(data) {
    this._http.post(api_endpoints + '/sub_category', {
      sub_category: {
        category_id: data.category_id,
        name: data.category_name,
      }
    }, this._header).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            this._router.navigate(['/admin/sub-categories']);
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log('Error occured');
      }
    );
  }

  update(data) {
    this._http.put(api_endpoints + '/sub_category/' + data.id, {
      sub_category: {
        category_id: data.category_id,
        name: data.category_name,
      }
    }, this._header).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            this._router.navigate(['/admin/sub-categories']);
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log('Error occured');
      }
    );
  }
}
