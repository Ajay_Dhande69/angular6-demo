import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  categories: any = null;
  sub_categories: any = null;
  product: any = null;
  _header: any = null;
  _file: any;
  prod_image: any = { 'size': '', 'type': '', 'file': '', 'fileName': '' };
  _prod_img: any;
  _prod_img_type: any = ['image/jpeg', 'image/png'];
  _prod_img_size_error: any;
  _prod_img_error: any = true;
  _prod_img_size: 2000000000;
  _prod_img_type_error: any;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }

  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getCategories();
    this.getProduct(this._activated_route.snapshot.params.product_id);
  }

  getCategories() {
    this._http.get(api_endpoints + '/category', this._header)
      .subscribe(data => {
        this.categories = JSON.parse(data['_body']).data;
      });
  }

  getProduct(id) {
    this._http.get(api_endpoints + '/product/' + id, this._header)
      .subscribe(data => {
        this.product = JSON.parse(data['_body']).data;
      });
  }

  getSubCategories(evt) {
    if (evt.target.value === '') {
      this.sub_categories = [];
    } else {
      this._http.get(api_endpoints + '/category/get_sub_categories?id=' + evt.target.value, this._header)
        .subscribe(data => {
          this.sub_categories = JSON.parse(data['_body']).data;
        });
    }
  }


  product_image(evt) {
    this._file = evt.target.files;
    const reader = new FileReader();
    const file: File = evt.target.files;
    this.prod_image['size'] = file[0].size;
    this.prod_image['type'] = file[0].type;
    this.prod_image['fileName'] = file[0].name;
    this._prod_img = file[0].name;
    if (this._prod_img_type.indexOf(file[0].type) < 0) {
      this._prod_img_type_error = null;
      this._prod_img_error = true;
      this._prod_img_type_error = 'Image type does not matched.';
      $('#faclt_new_faculty').attr('disabled', true);
    } else if (this._prod_img_size <= file[0].size) {
      this._prod_img_size_error = null;
      this._prod_img_error = true;
      this._prod_img_size_error = 'Image size does not matched.';
      $('#faclt_new_faculty').attr('disabled', true);
    } else {
      this.prod_image = { 'size': '', 'type': '', 'file': '', 'fileName': '' };
      this._prod_img_size_error = null;
      this._prod_img_type_error = null;
      this.prod_image['size'] = file[0].size;
      this.prod_image['type'] = file[0].type;
      this.prod_image['fileName'] = file[0].name;
      reader.readAsDataURL(file[0]);
      reader.onload = () => {
        $('#prev_prod_img').attr('src', reader.result);
        this.prod_image['file'] = reader.result;
      };
      this._prod_img_error = false;
    }
  }

  onProductFormSubmit(data) {
    if (data.id === '') {
      this.create(data);
    } else {
      this.update(data);
    }
  }

  create(data) {
    this._http.post(api_endpoints + '/product', {
      product: {
        name: data.product_name,
        category_id: data.product_category_id,
        sub_category_id: data.product_sub_category_id,
        image: this.prod_image,
        rating: data.product_rating,
        description: data.product_description,
        off: data.product_offer,
        price: data.product_price,
        brand: data.product_brand,
        max_quantity: data.product_max_qty
      }
    }, this._header).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            this._router.navigate(['/admin/products']);
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log('Error occured');
      }
    );
  }

  update(data) {

  }

}
