import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderdProductComponent } from './orderd-product.component';

describe('OrderdProductComponent', () => {
  let component: OrderdProductComponent;
  let fixture: ComponentFixture<OrderdProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderdProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderdProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
