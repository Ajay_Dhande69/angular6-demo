import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  category: any = null;
  _header: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getCategory(this._activated_route.snapshot.params.category_id);
  }

  // Edit Category
  getCategory(id) {
    this._http.get(api_endpoints + '/category/' + id, this._header)
      .subscribe(data => {
        this.category = JSON.parse(data['_body']).data;
      });
  }

  // Create Category
  onCategoryFormSubmit(data) {
    if (data.id === '') {
      this.create(data);
    } else {
      this.update(data);
    }
  }

  create(data) {
    this._http.post(api_endpoints + '/category', {
      category: {
        name: data.category_name,
      }
    }, this._header).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            this._router.navigate(['/admin/categories']);
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log('Error occured');
      }
    );
  }

  update(data) {
    this._http.put(api_endpoints + '/category/' + data.id, {
      category: {
        name: data.category_name,
      }
    }, this._header).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            this._router.navigate(['/admin/categories']);
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log('Error occured');
      }
    );
  }

}
