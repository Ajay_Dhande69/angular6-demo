import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})

export class ListOrdersComponent implements OnInit {
  _header: any = null;
  _loading = false;
  orders: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getOrders();
  }

  getOrders() {
    this._loading = true;
    this._http.get(api_endpoints + '/order', this._header)
      .subscribe(data => {
        this.orders = JSON.parse(data['_body']).data;
        setTimeout(function () {
          $(function () {
            $('#orders_list').DataTable();
          });
        }, 0);
      });
    this._loading = false;
  }
}
