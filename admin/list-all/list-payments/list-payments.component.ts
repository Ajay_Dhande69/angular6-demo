import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-list-payments',
  templateUrl: './list-payments.component.html',
  styleUrls: ['./list-payments.component.css']
})
export class ListPaymentsComponent implements OnInit {
  _header: any = null;
  _loading = false;
  payments: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getPayments();
  }

  getPayments() {
    this._loading = true;
    this._http.get(api_endpoints + '/payment', this._header)
      .subscribe(data => {
        this.payments = JSON.parse(data['_body']).data;
        setTimeout(function () {
          $(function () {
            $('#payments_list').DataTable();
          });
        }, 0);
        this._loading = false;
      });
  }

}
