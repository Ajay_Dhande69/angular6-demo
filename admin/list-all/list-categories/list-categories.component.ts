import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.css']
})

export class ListCategoriesComponent implements OnInit {
  categories: any = null;
  _header: any = null;
  _loading = false;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getCategories();
  }

  // Get all Categories
  getCategories() {
    this._loading = true;
    this._http.get(api_endpoints + '/category', this._header)
      .subscribe(data => {
        this.categories = JSON.parse(data['_body']).data;
        setTimeout(function () {
          $(function () {
            $('#categories_list').DataTable();
          });
        }, 0);
        this._loading = false;
      });
  }

  // Delete Category
  delete_category(id) {
    Swal({
      title: 'Are you sure?',
      text: 'Do you really want to delete this Category?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        return this._http.delete(api_endpoints + '/category/' + id, this._header).subscribe((resp: Response) => {
          Swal({
            title: 'Thank You',
            text: JSON.parse(resp['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((res) => {
            this._router.navigate([this._router.url]);
          });
        });
      } else {
        return false;
      }
    });
  }

}
