import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOrderdProductsComponent } from './list-orderd-products.component';

describe('ListOrderdProductsComponent', () => {
  let component: ListOrderdProductsComponent;
  let fixture: ComponentFixture<ListOrderdProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOrderdProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOrderdProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
