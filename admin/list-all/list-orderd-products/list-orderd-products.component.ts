import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-orderd-products',
  templateUrl: './list-orderd-products.component.html',
  styleUrls: ['./list-orderd-products.component.css']
})
export class ListOrderdProductsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
