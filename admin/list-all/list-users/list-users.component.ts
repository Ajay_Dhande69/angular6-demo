import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  _header: any = null;
  users: any = null;
  _loading = false;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getUsers();
  }

  getUsers() {
    this._loading = true;
    this._http.get(api_endpoints + '/authentication', this._header)
      .subscribe(data => {
        this.users = JSON.parse(data['_body']).data;
        setTimeout(function () {
          $(function () {
            $('#users_list').DataTable();
          });
        }, 0);
        this._loading = false;
      });
  }

}
