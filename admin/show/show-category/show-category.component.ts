import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-show-category',
  templateUrl: './show-category.component.html',
  styleUrls: ['./show-category.component.css']
})

export class ShowCategoryComponent implements OnInit {
  _header: any = null;
  category: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getCategory(this._activated_route.snapshot.params.category_id);
  }
  getCategory(id) {
    this._http.get(api_endpoints + '/category/' + id, this._header)
      .subscribe(data => {
        this.category = JSON.parse(data['_body']).data;
      });
  }

}
