import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-show-payment',
  templateUrl: './show-payment.component.html',
  styleUrls: ['./show-payment.component.css']
})
export class ShowPaymentComponent implements OnInit {
  _header: any = null;
  payment: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getPayment(this._activated_route.snapshot.params.payment_id);
  }

  getPayment(id) {
    this._http.get(api_endpoints + '/payment/' + id, this._header)
      .subscribe(data => {
        this.payment = JSON.parse(data['_body']).data;
      });
  }

}
