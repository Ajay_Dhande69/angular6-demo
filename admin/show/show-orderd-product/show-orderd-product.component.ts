import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-orderd-product',
  templateUrl: './show-orderd-product.component.html',
  styleUrls: ['./show-orderd-product.component.css']
})
export class ShowOrderdProductComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

}
