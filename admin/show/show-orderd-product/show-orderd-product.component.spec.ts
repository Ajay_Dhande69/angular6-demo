import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOrderdProductComponent } from './show-orderd-product.component';

describe('ShowOrderdProductComponent', () => {
  let component: ShowOrderdProductComponent;
  let fixture: ComponentFixture<ShowOrderdProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowOrderdProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOrderdProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
