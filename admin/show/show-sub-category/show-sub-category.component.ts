import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-show-sub-category',
  templateUrl: './show-sub-category.component.html',
  styleUrls: ['./show-sub-category.component.css']
})
export class ShowSubCategoryComponent implements OnInit {
  _header: any = null;
  sub_category: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getSubCategory(this._activated_route.snapshot.params.sub_category_id);
  }

  getSubCategory(id) {
    this._http.get(api_endpoints + '/sub_category/' + id, this._header)
      .subscribe(data => {
        this.sub_category = JSON.parse(data['_body']).data;
      });
  }


}
