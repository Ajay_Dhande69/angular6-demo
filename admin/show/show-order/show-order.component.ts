import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../../app/api_endpoints';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-show-order',
  templateUrl: './show-order.component.html',
  styleUrls: ['./show-order.component.css']
})
export class ShowOrderComponent implements OnInit {

  _header: any = null;
  order: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.getOrder(this._activated_route.snapshot.params.order_id);
  }

  getOrder(id) {
    this._http.get(api_endpoints + '/order/' + id, this._header)
      .subscribe(data => {
        this.order = JSON.parse(data['_body']).data;
        console.log(this.order);
      });
  }

}
