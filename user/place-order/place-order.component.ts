import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../app/api_endpoints';
import { AppComponent } from '../../app.component';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css']
})
export class PlaceOrderComponent implements OnInit {
  _header: any = null;
  user: any = null;
  cart_items: any = null;
  total_prise: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.user = JSON.parse(window.sessionStorage.getItem('user_detail'));
    if (window.localStorage.getItem('cart_data') !== null) {
      this.cart_items = JSON.parse('[' + window.localStorage.getItem('cart_data') + ']');
      this.totalPrice();
    }
  }

  totalPrice() {
    this.cart_items.forEach(ele => {
      this.total_prise = this.total_prise + ele.tot_price;
    });
  }

  orderCheckout() {
    const header = this._header;
    const http = this._http;
    const user = this.user;
    const tot_price = this.total_prise;
    const cart_products = this.cart_items;
    const handler = (<any>window).StripeCheckout.configure({
      key: 'pk_test_oi0sKPJYLGjdvOXOM8tE8cMa',
      image: this.user.picture,
      locale: 'auto',
      token: function (token: any) {

        http.post(api_endpoints + '/order/create_order_payment', {
          order: {
            id: user.id,
            orderd_products: cart_products,
            payment_number: 'PAY_' + String(Math.random()).replace('.', ''),
            total_price: tot_price,
            token_id: token.id,
            card_id: token.card.id,
            brand: token.card.brand,
            card_type: token.card.type,
            exp_month: token.card.exp_month,
            exp_year: token.card.exp_year,
            last4: token.card.last4,
            email: token.email,
            status: true
          }
        }, header).subscribe(
          res => {
            if (JSON.parse(res['_body']).status === 200) {
              window.localStorage.removeItem('cart_data');
              Swal({
                title: 'Thank You\n' + JSON.parse(res['_body']).order_id,
                text: JSON.parse(res['_body']).message,
                type: 'success',
                confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.href = '';
              });
            } else {
              Swal({
                title: 'Warning',
                text: JSON.parse(res['_body']).message,
                type: 'warning',
                confirmButtonText: 'Ok'
              });
            }
          },
          err => {
            console.log('Error occured');
          }
        );
      }
    });

    handler.open({
      name: this.user.first_name + ' ' + this.user.last_name,
      description: 'Make a transac' + String(this.cart_items.length) + ' Products',
      amount: this.total_prise * 100
    });
  }


  onChangeAddress(data) {
    this._http.post(api_endpoints + '/authentication/update_address', {
      user: {
        id: this.user.id,
        address: data.sign_up_address
      }
    }, this._header).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          window.sessionStorage.setItem('user_detail', JSON.stringify(JSON.parse(res['_body']).data));
          this.user = JSON.parse(window.sessionStorage.getItem('user_detail'));
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log('Error occured');
      }
    );
  }

}
