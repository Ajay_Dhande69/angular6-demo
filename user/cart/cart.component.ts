import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../app/api_endpoints';
import { AppComponent } from '../../app.component';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  product: any = null;
  quantity: any = 1;
  cart_items: any = null;
  total_prise: any = null;
  cart: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  _appcomponent = new AppComponent(this._router);
  ngOnInit() {
    window.scrollTo(0, 0);
    if (window.localStorage.getItem('cart_data') !== null) {
      this.cart_items = JSON.parse('[' + window.localStorage.getItem('cart_data') + ']');
      this.totalPrice();
    }
  }

  totalPrice() {
    this.total_prise = null;
    this.cart_items.forEach(ele => {
      this.total_prise = this.total_prise + ele.tot_price;
    });
  }

  // Update quantity of the item.
  increaseQuantity(value, cart_item) {
    this.total_prise = null;
    // tslint:disable-next-line:prefer-const
    this.cart = [];
    window.localStorage.removeItem('cart_data');
    this.cart_items.forEach(ele => {
      if (ele.id === cart_item.id) {
        ele.tot_price = value * cart_item.price;
        ele.quantity = value;
      }
      this.total_prise = this.total_prise + ele.tot_price;
      this.cart.push(JSON.stringify(ele));
    });
    window.localStorage.setItem('cart_data', this.cart);
  }

  // Remove item in cart.
  removeItem(id) {
    Swal({
      title: 'Are you sure?',
      text: 'Do you really want to remove this product from cart?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {

        Swal({
          title: 'Deleted!',
          text: 'Product has been deleted successfully.',
          type: 'success',
          confirmButtonText: 'Ok'
        }).then((res) => {
          // tslint:disable-next-line:prefer-const
          let new_cart_items = (this.cart_items.filter(function (obj) {
            if (obj.id !== id) {
              return obj;
            }
          }));

          if (new_cart_items.length === 0) {
            window.localStorage.removeItem('cart_data');
            this.cart_items = null;
            $('.cart-item-counts').text('0');
          } else {
            window.localStorage.removeItem('cart_data');
            window.localStorage.setItem('cart_data', JSON.stringify(new_cart_items).replace(']', '').replace('[', ''));
            this.cart_items = new_cart_items;
            this.totalPrice();
          }
        });
      } else {
        return false;
      }
    });
  }

  placeOrder() {
    if (window.sessionStorage.getItem('session_token') === null) {
      Swal({
        title: 'Warning!',
        text: 'Please sign in to continue the checkout process.',
        type: 'warning',
        confirmButtonText: 'Ok'
      }).then((result) => {
        this._router.navigate(['/user/sign-in']);
      });
    } else {
      this._router.navigate(['/user/placed-order']);
    }
  }

}
