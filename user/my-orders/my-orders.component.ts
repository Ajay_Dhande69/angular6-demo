import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../app/api_endpoints';
import { AppComponent } from '../../app.component';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {
  _header: any = null;
  user: any = null;
  orders: any = null;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  ngOnInit() {
    window.scrollTo(0, 0);
    // tslint:disable-next-line:max-line-length
    this._header = { headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': window.sessionStorage.getItem('session_token') }) };
    this.user = JSON.parse(window.sessionStorage.getItem('user_detail'));
    this.getOrders();
  }

  getOrders() {
    this._http.get(api_endpoints + '/order/get_my_orders', this._header)
      .subscribe(data => {
        this.orders = JSON.parse(data['_body']).data;
        console.log(this.orders);
      });
  }

}
