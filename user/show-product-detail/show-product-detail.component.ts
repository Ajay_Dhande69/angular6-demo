import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../app/api_endpoints';
import { AppComponent } from '../../app.component';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-show-product-detail',
  templateUrl: './show-product-detail.component.html',
  styleUrls: ['./show-product-detail.component.css']
})
export class ShowProductDetailComponent implements OnInit {
  product: any = null;
  quantity: any = 1;
  cart: any = null;
  dup_product: any = false;
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  _appcomponent = new AppComponent(this._router);
  ngOnInit() {
    window.scrollTo(0, 0);
    const s = document.createElement('script');
    s.src = 'https://http-localhost-4200-13.disqus.com/embed.js';
    // s.setAttribute('data-timestamp', + new Date());
    (document.head || document.body).appendChild(s);

    this.getProduct(this._activated_route.snapshot.params.product_id);
  }

  getProduct(id) {
    this._http.get(api_endpoints + '/get_product?id=' + id)
      .subscribe(data => {
        this.product = JSON.parse(data['_body']).data;
      });
  }

  add_to_cart(quantity) {
    const cart_items = JSON.parse('[' + window.localStorage.getItem('cart_data') + ']');
    const cart = JSON.stringify($.extend(this.product[0], {
      image: this.product[1],
      category: this.product[2],
      sub_category: this.product[3],
      quantity: quantity,
      tot_price: this.product[0].price * quantity
    }));
    if (window.localStorage.getItem('cart_data') === null) {
      window.localStorage.setItem('cart_data', cart);
      $('.cart-item-counts').text(cart_items.length);
      Swal({
        title: 'Thank You',
        text: 'Product added in the cart.',
        type: 'success',
        confirmButtonText: 'Ok'
      }).then((res) => {
        this._router.navigate(['/user/cart']);
      });
    } else {
      cart_items.forEach(ele => {
        if (this.product[0].id === ele.id) {
          this.dup_product = true;
        }
      });
      if (this.dup_product === true) {
        Swal({
          title: 'Warning',
          text: 'Product is already added in the cart.',
          type: 'warning',
          confirmButtonText: 'Ok'
        });
      } else {
        this.cart = [];
        this.cart.push(window.localStorage.getItem('cart_data'));
        this.cart.push(cart);
        window.localStorage.setItem('cart_data', this.cart);
        $('.cart-item-counts').text(JSON.parse('[' + window.localStorage.getItem('cart_data') + ']').length);
        Swal({
          title: 'Thank You',
          text: 'Product added in the cart.',
          type: 'success',
          confirmButtonText: 'Ok'
        }).then((res) => {
          this._router.navigate(['/user/cart']);
        });
      }
    }
  }
}
