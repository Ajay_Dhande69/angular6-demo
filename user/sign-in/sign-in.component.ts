import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { NgModel } from '@angular/forms';
import { api_endpoints } from '../../../app/api_endpoints';
import { AppComponent } from '../../app.component';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

export class SignInComponent implements OnInit {
  constructor(private _activated_route: ActivatedRoute, private _http: Http, private _router: Router) { }
  _appcomponent = new AppComponent(this._router);
  ngOnInit() {
    window.scrollTo(0, 0);
  }
  on_signin_submit(data) {
    this._http.post(api_endpoints + '/authentication/authenticate', {
      'user':
      {
        'email': data.sign_in_email,
        'password': data.sign_in_password
      }
    }).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          const json_body = JSON.parse(res['_body']);
          window.sessionStorage.setItem('session_token', ((json_body.auth_token).toString()));
          window.sessionStorage.setItem('user_detail', JSON.stringify(json_body.data));

          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            this._appcomponent.ngOnInit();
            window.location.href = '';
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  on_forgot_password_submit(data) {
    this._http.post(api_endpoints + '/authentication/forget_password', {
      'user':
      {
        'email': data.fp_email
      }
    }).subscribe(
      res => {
        if (JSON.parse(res['_body']).status === 200) {
          Swal({
            title: 'Thank You',
            text: JSON.parse(res['_body']).message,
            type: 'success',
            confirmButtonText: 'Ok'
          }).then((result) => {
            $('.forget_password_close').click();
            this._router.navigate(['/']);
            // this._appcomponent.ngOnInit();
            // window.location.href = '';
          });
        } else {
          Swal({
            title: 'Warning',
            text: JSON.parse(res['_body']).message,
            type: 'warning',
            confirmButtonText: 'Ok'
          });
        }
      },
      err => {
        console.log(err);
      }
    );
  }
}
