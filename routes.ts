import { RouterModule, Routes } from '@angular/router';
import { DefaultComponent } from './default/default.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserLayoutComponent } from './layout/user-layout/user-layout.component';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { UserAuthGuard } from './user-auth.guard';
import { LoggedUserGuard } from './logged-user.guard';
import { AdminGuard } from './admin.guard';
import { UserComponent } from './admin/new-edit-form/user/user.component';
import { OrderComponent } from './admin/new-edit-form/order/order.component';
import { CategoryComponent } from './admin/new-edit-form/category/category.component';
import { CommentComponent } from './admin/new-edit-form/comment/comment.component';
import { OrderdProductComponent } from './admin/new-edit-form/orderd-product/orderd-product.component';
import { ProductComponent } from './admin/new-edit-form/product/product.component';
import { SubCategoryComponent } from './admin/new-edit-form/sub-category/sub-category.component';
import { PaymentComponent } from './admin/new-edit-form/payment/payment.component';
import { ShowUserComponent } from './admin/show/show-user/show-user.component';
import { ShowCategoryComponent } from './admin/show/show-category/show-category.component';
import { ShowSubCategoryComponent } from './admin/show/show-sub-category/show-sub-category.component';
import { ShowPaymentComponent } from './admin/show/show-payment/show-payment.component';
import { ShowProductComponent } from './admin/show/show-product/show-product.component';
import { ShowOrderComponent } from './admin/show/show-order/show-order.component';
import { ShowOrderdProductComponent } from './admin/show/show-orderd-product/show-orderd-product.component';
import { ShowCommentComponent } from './admin/show/show-comment/show-comment.component';
import { ListUsersComponent } from './admin/list-all/list-users/list-users.component';
import { ListCommentsComponent } from './admin/list-all/list-comments/list-comments.component';
import { ListOrdersComponent } from './admin/list-all/list-orders/list-orders.component';
import { ListSubCategoriesComponent } from './admin/list-all/list-sub-categories/list-sub-categories.component';
import { ListCategoriesComponent } from './admin/list-all/list-categories/list-categories.component';
import { ListProductsComponent } from './admin/list-all/list-products/list-products.component';
import { ListOrderdProductsComponent } from './admin/list-all/list-orderd-products/list-orderd-products.component';
import { ListPaymentsComponent } from './admin/list-all/list-payments/list-payments.component';
import { ShowProductDetailComponent } from './user/show-product-detail/show-product-detail.component';
import { CartComponent } from './user/cart/cart.component';
import { PlaceOrderComponent } from './user/place-order/place-order.component';
import { ProfileComponent } from './user/profile/profile.component';
import { MyOrdersComponent } from './user/my-orders/my-orders.component';
import { AboutUsComponent } from './user/about-us/about-us.component';
import { ContactUsComponent } from './user/contact-us/contact-us.component';
import { ForgotPasswordComponent } from './user/forgot-password/forgot-password.component';

export const app_routes: Routes = [
  { path: '', component: DefaultComponent },
  { path: 'user/sign-in', canActivate: [UserAuthGuard], component: SignInComponent },
  { path: 'user/sign-up', canActivate: [UserAuthGuard], component: SignUpComponent },
  { path: 'user/reset-password', canActivate: [UserAuthGuard], component: ForgotPasswordComponent },
  { path: 'user/placed-order', component: PlaceOrderComponent },
  { path: 'user/my-profile', canActivate: [LoggedUserGuard], component: ProfileComponent },
  { path: 'user/my-orders', canActivate: [LoggedUserGuard], component: MyOrdersComponent },
  { path: 'user/about-us', component: AboutUsComponent },
  { path: 'user/contact-us', component: ContactUsComponent },
  { path: 'admin/users', component: ListUsersComponent },
  { path: 'admin/comments', component: ListCommentsComponent },
  { path: 'admin/orders', component: ListOrdersComponent },
  { path: 'admin/sub-categories', canActivate: [AdminGuard], component: ListSubCategoriesComponent },
  { path: 'admin/categories', canActivate: [AdminGuard], component: ListCategoriesComponent },
  { path: 'admin/orderd-products', canActivate: [AdminGuard], component: ListOrderdProductsComponent },
  { path: 'admin/payments', canActivate: [AdminGuard], component: ListPaymentsComponent },
  { path: 'admin/products', canActivate: [AdminGuard], component: ListProductsComponent },
  {
    path: 'admin/categories',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: CategoryComponent
      }, {
        path: 'edit/:category_id',
        canActivate: [AdminGuard],
        component: CategoryComponent
      }, {
        path: 'show/:category_id',
        canActivate: [AdminGuard],
        component: ShowCategoryComponent
      }
    ]
  },
  {
    path: 'admin/users',
    children: [
      {
        path: 'edit/:user_id',
        canActivate: [AdminGuard],
        component: UserComponent
      }, {
        path: 'show/:user_id',
        canActivate: [AdminGuard],
        component: ShowUserComponent
      }
    ]
  },
  {
    path: 'admin/comments',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: CommentComponent
      }, {
        path: 'edit/:comment_id',
        canActivate: [AdminGuard],
        component: CommentComponent
      }, {
        path: 'show/:comment_id',
        canActivate: [AdminGuard],
        component: ShowCommentComponent
      }
    ]
  },
  {
    path: 'admin/orders',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: OrderComponent
      }, {
        path: 'edit/:order_id',
        canActivate: [AdminGuard],
        component: OrderComponent
      }, {
        path: 'show/:order_id',
        canActivate: [AdminGuard],
        component: ShowOrderComponent
      }
    ]
  },
  {
    path: 'admin/sub-categories',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: SubCategoryComponent
      }, {
        path: 'edit/:sub_category_id',
        canActivate: [AdminGuard],
        component: SubCategoryComponent
      }, {
        path: 'show/:sub_category_id',
        canActivate: [AdminGuard],
        component: ShowSubCategoryComponent
      }
    ]
  },
  {
    path: 'admin/orderd-products',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: OrderdProductComponent
      }, {
        path: 'edit/:orderd_product_id',
        canActivate: [AdminGuard],
        component: OrderdProductComponent
      }, {
        path: 'show/:orderd_product_id',
        canActivate: [AdminGuard],
        component: ShowOrderdProductComponent
      }
    ]
  },
  {
    path: 'admin/payments',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: PaymentComponent
      }, {
        path: 'edit/:payment_id',
        canActivate: [AdminGuard],
        component: PaymentComponent
      }, {
        path: 'show/:payment_id',
        canActivate: [AdminGuard],
        component: ShowPaymentComponent
      }
    ]
  },
  {
    path: 'admin/products',
    children: [
      {
        path: 'new',
        canActivate: [AdminGuard],
        component: ProductComponent
      }, {
        path: 'edit/:product_id',
        canActivate: [AdminGuard],
        component: ProductComponent
      }, {
        path: 'show/:product_id',
        canActivate: [AdminGuard],
        component: ShowProductComponent
      }
    ]
  },
  {
    path: 'user/product-details/:product_id', component: ShowProductDetailComponent
  },
  {
    path: 'user/cart', component: CartComponent
  },
  { path: '**', component: PageNotFoundComponent }
];
